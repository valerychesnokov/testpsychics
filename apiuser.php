<?php
	ini_set('display_errors', 1); // DEBUG!
	error_reporting(E_ALL); // DEBUG!

	spl_autoload_register(function ($class_name) {
	    include $class_name . '.php';
	});

	// required headers
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");

	$outArr = array();
	$outArr["error"] = null;

	try {
		// make out result JSON array
		if (!isset($_GET["userkey"]))
		{
    		$outArr["error"] = "ERROR: expected parameter: userkey (string)";
			echo json_encode($outArr, true);
    		exit();
		}

		if (!isset($_GET["count"]))
		{
    		$outArr["error"] = "ERROR: expected parameter: count (natural positive)";
			echo json_encode($outArr, true);
    		exit();
		}

		if (!isset($_GET["number"]))
		{
    		$outArr["error"] = "ERROR: expected parameter: number (10-99)";
			echo json_encode($outArr, true);
    		exit();
		}
		
		$count = $_GET["count"];
		$number = $_GET["number"];
		$userkey = $_GET["userkey"];

    	$outArr["number"] = $number;
    	$outArr["data"] = array();
    	///$outArr["data"]["guess"] = array();
    	///$outArr["data"]["raiting"] = array();

		for ($i = 0; $i < $count; $i++)
		{
			$val = rand (10, 99);
    		
    		$outArr["data"][$i] = array();

   			$outArr["data"][$i]["p_number"] = $val;

			if ($val == $number)
    			$outArr["data"][$i]["p_raiting"] = 1;
    		else
    			$outArr["data"][$i]["p_raiting"] = -1;
		}

		// convert array to JSON string
		$strJsonResult = json_encode($outArr, true);

		// Save item's Data to Database
		User::putUserData($userkey, $strJsonResult);

		// SEND RESULT STRING
		echo $strJsonResult;
		// EXIT, DONE
		exit();
	} 
	catch (Exception $e) {
		$outArr["error"] = $e->getMessage();
		exit();
	}
	finally {
		echo json_encode($outArr, true);
	}	
?>