<?php
	class Db {
		public static function getConnect() {
			//return (new PDO("mysql:host=localhost;dbname=pikmlm_testdrive;","pikmlm_testdrive","111222333"));

			// Create (connect to) SQLite database in file
			$pdo = new PDO(
			    'sqlite::memory:',
			    null,
			    null,
			    array(PDO::ATTR_PERSISTENT => true, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
			); 					

			if ( !isset($pdo) || is_null($pdo) ) {
				  throw new Exception("Can't create PDO('sqlite::memory:') database");;
				  return;
			}

			//$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo->setAttribute(PDO::ATTR_ERRMODE, 
                      PDO::ERRMODE_WARNING);

			return $pdo;
    	}
	}
?>